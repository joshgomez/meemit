<?php

	if(!defined('IN_SITE'))
	{
		exit;
	}
	
?>

<nav class='pagelinks'>
	<ul>
		<li><?=_translate('PAGES')?>:</li>
		<?=$output_pageLinks?>
	</ul>
</nav>