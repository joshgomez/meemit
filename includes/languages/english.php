<?php

	if(!IN_SITE)
	{
		exit;
	}
	
	$lang['HTML_KEYWORDS'] = 'Meemit,Meme,Internet,XzaR,Josh,Xuniver,Troll,Epic,Slang,Stupid,Anything,Whatever,Cool';
	$lang['HTML_DESCRIPTION'] = 'A simplified reddit clone written in PHP.';
	$lang['TXT_SLOGAN'] = 'every corner on the Internet.';
	
	$lang['HTML_DESCRIPTION_HOME']			= 'This is the front page of meemit. Links contributed by users can be seen here.';
	$lang['HTML_DESCRIPTION_PROFILE'] 		= 'This is %s\' profile page. User statistics, information and links contributed by this user can be seen here.';
	$lang['HTML_DESCRIPTION_CATEGORIES'] 	= 'This is a list of all Meemits categories. A short description and total contributions made for each category.';
	$lang['HTML_DESCRIPTION_REGISTER']		= 'This is the registration and login page for Meemit.';
	
	$lang['SEARCH'] = 'Search';
	$lang['CONTRIBUTE'] = 'Contribute';
	$lang['LOGIN'] = 'Login';
	$lang['JOIN'] = 'Join';
	$lang['CHANGE'] = 'Change';
	$lang['FRONT'] = 'Front';
	$lang['SETTINGS'] = 'Settings';
	$lang['MORE'] = 'MORE';
	$lang['LINK'] = 'Link';
	$lang['TITLE'] = 'Title';
	$lang['URL'] = 'Url';
	$lang['VOTE'] = 'Vote';
	$lang['STATISTICS'] = 'Statistics';
	$lang['CONTRIBUTED'] = 'Contributed';
	$lang['CONTRIBUTIONS'] = 'Contributions';
	$lang['KARMA_CONTRIBUTION'] = 'Contribution karma';
	$lang['VOTES_CONTRIBUTION'] = 'Contribution votes';
	$lang['PAGES'] = 'Pages';
	
	$lang['DELETE_CONTRIBUTION'] = 'Delete contribution';
	$lang['DELETE'] = 'Delete';
	
	$lang['PRIVACY'] = 'Privacy';
	$lang['SHOW_FIRSTNAME'] = 'Show firstname';
	$lang['SHOW_LASTNAME'] = 'Show lastname';
	
	$lang['DESCRIPTION'] = 'Description';
	$lang['INFORMATION'] = 'Information';
	$lang['UNKNOWN'] = 'Unknown';
	
	$lang['VOTE_UP'] = "Vote up";
	$lang['VOTE_DOWN'] = "Vote down";
	
	$lang['ONLINE'] = 'Online';
	$lang['OFFLINE'] = 'Offline';
	
	$lang['EMPTY'] = 'Empty';
	$lang['FIRST'] = 'First';
	$lang['PREVIOUS'] = 'Previous';
	
	$lang['NEXT'] = 'Next';
	$lang['LAST'] = 'Last';
	
	$lang['HOT'] = 'Hot';
	$lang['NEW'] = 'New';
	$lang['RISING'] = 'Rising';
	$lang['TOP'] = 'Top';
	$lang['CONTROVERSIAL'] = 'Controversial';

	$lang['FIRSTNAME'] 	= 'First name';
	$lang['LASTNAME'] 	= 'Last name';
	
	$lang['PROFILE'] = 'Profile';
	$lang['AUTHENTICATE'] = 'Authenticate';
	$lang['LOGOUT'] = 'Logout';
	$lang['CAPTCHA'] = 'Captcha';
	$lang['USERNAME'] = 'Username';
	$lang['USER'] = 'User';
	$lang['EMAIL'] = 'Email';
	$lang['PASSWORD'] = 'Password';
	$lang['REPEAT_PASSWORD'] = 'Repeat password';
	$lang['OLD_PASSWORD'] = 'Old password';
	$lang['ENTER_CODE'] = 'Enter Code';
	
	$lang['OPTIONAL'] = 'Optional';
	
	$lang['OPENID'] = 'OpenID';
	$lang['UNLINKED'] = 'Not Linked';
	$lang['LINKED'] = 'Linked';
	$lang['GOOGLE'] = 'Google';
	
	$lang['TIT_LOGIN_REG'] = 'Login or create a new account';
	$lang['TIT_CREATE_ACCOUNT'] = 'Create a new account';
	$lang['TIT_MORE_ABOUT'] = 'More about you';

	$lang['QSN_HAVE_ACCOUNT'] = 'Already have an account?';
	$lang['QSN_JOIN'] = 'Want to join?';
	$lang['QSN_MISSPELLED'] = 'Misspelled?';
	$lang['QSN_PASSWORD'] = 'Weak password?';
	$lang['QSN_SHARE'] = 'Share something?';
	$lang['QSN_OPENID'] = 'Login with third parts?';
	$lang['QSN_MEEMIT'] = 'What is Meemit?';
	
	$lang['TXT_USER_CONTRIBUTIONS'] = 'A list of meemits made by this user.';
	$lang['TXT_CATEGORIES'] 		= 'A list of all categories.';

	$lang['TXT_PROFILE_SAVED'] = 'Profile have been saved!';
	
	$lang['TXT_SPAM'] = 'Please don\'t abuse the refresh command or double click on links many time in row. <a href="">Go Back</a><br />Spammed: %d Total: %d';
	
	$lang['TXT_SAME_AS'] = 'Same as %s.';
	
	$lang['TXT_SQL_ERROR'] = 'PDO::SQL->Error: (#%d) %s';
	$lang['TXT_SQL_PUBLIC_ERROR'] = 'PDO::SQL->Error: Something went wrong!';
	
	$lang['TXT_INVALID_CAPTCHA'] = 'Invalid captcha code entered.';
	$lang['TXT_CAPTCHA_PLACEHOLDER'] = 'Are you human?';
	
	$lang['TXT_PASSWORD_WRONG'] = 'Your password is wrong.';
	$lang['TXT_USERNAME_EXISTS'] = 'A user with same username already exists.';
	$lang['TXT_EMAIL_EXISTS'] = 'A user with same e-mail address already exists.';
	$lang['TXT_OPENID_NOLINK'] = 'A user already exists with same e-mail address provided by openid.';
	
	$lang['TXT_CATEGORY_NONE'] = 'The submeemit could not be found.';

	$lang['TXT_PASSWORD_MISSPELLED'] 		= 'The password is misspelled.';
	$lang['TXT_PASSWORD_INCLUDE_NUM'] 		= 'The password must at least contain one number.';
	$lang['TXT_PASSWORD_INCLUDE_L'] 		= 'The password must at least contain one letter.';
	$lang['TXT_PASSWORD_INCLUDE_UCL'] 		= 'The password must at least contain one upper case letter.';
	$lang['TXT_PASSWORD_INCLUDE_LCL'] 		= 'The password must at least contain one lower case letter.';
	$lang['TXT_PASSWORD_INCLUDE_SYMBOL'] 	= 'The password must at least contain one symbol.';
	
	$lang['TXT_INVALID_OPENID'] = 'Invalid OpenID provider.';
	$lang['TXT_OPENID_UNLINK_REQ'] = 'You must at least have one more OpenID provider or a password to unlink this provider.';
	$lang['TXT_OPENID_EMAIL_MISMATCH'] = 'Email address provided by third part does not match your account\'s email.';

	$lang['TXT_INVALID_NAME'] = 'The name is invalid. Letters are allowed followed by one dot, hyphen or space.';
	$lang['TXT_INVALID_USERNAME'] = 'The username is invalid. Letters and numbers allowed followed by one underscore or hyphen.';
	$lang['TXT_INVALID_LOGIN'] = 'Sorry, no user could be found. :( Please try again.';
	$lang['TXT_INVALID_EMAIL'] = 'The e-mail address is invalid.';
	$lang['TXT_INVALID_EMAIL_DOMAIN'] = 'The domain for the e-mail address is invalid.';
	$lang['TXT_INVALID_URL'] = 'The url is invalid.';
	
	$lang['TXT_VALID_USERNAME'] = 'Letters and numbers allowed followed by one underscore or hyphen.';
	$lang['TXT_VALID_NAME'] = 'Letters allowed followed by one dot, hyphen or space.';
	$lang['TXT_VALID_PASSWORD'] = 'Must contain at least one letter and number.';
	
	$lang['TXT_MIN_LENGTH'] = 'Minimum length %d characters.';
	$lang['TXT_MAX_LENGTH'] = 'Maximum length %d characters.';
	
	$lang['TXT_INPUT_TOO_SHORT'] = 'The %s you have entered is too short. Minimum length %d characters.';
	$lang['TXT_INPUT_TOO_LONG'] = 'The %s you have entered is too long. Maximum length %d characters.';
	
	
	$lang['TXT_DESC_REGISTER'] = 'All you need is to fill in this fields or sign in using OpenID.';
	$lang['TXT_DESC_LOGIN'] = 'Sign in by using password or OpenID.';
	$lang['TXT_DESC_SETTINGS'] = 'Complete your user information or change it.';
	$lang['TXT_DESC_PASSWORD'] = 'Set your password or change it if you already have one.';
	$lang['TXT_DESC_POST'] = 'The key to a successful submission is interesting content and a descriptive title.';
	$lang['TXT_DESC_OPENID'] = 'Link or unlink your account to a OpenId provider, must be same email address.';
	
	$lang['TXT_DESC_LOGIN'] = 'Sign in by using password or OpenID.';
	
	$lang['TXT_DESC_MEEMIT'] = " Meemit is a source for what's up to date and popular on the web. 
		Users like you provide all of the content and choose, through voting, what's good and what's off. 
		The name was inspired from the word meme.";
	
	$lang['TXT_CONTRIBUTIONS_NONE'] = 'No meemit contributions have been made.';
	$lang['TXT_CATEGORIES_NONE'] = 'No submeemits could be found.';
	
	$lang['LGN_USERNAME_EMAIL'] = 'Username or Email';
	$lang['LNK_LOGIN_REGISTER'] = 'Want to join? <a href="%s">Login or register</a> in seconds.';
	$lang['LNK_GOOGLE'] = 'Sign in with Google';
	$lang['LNK_SUBMIT_LINK'] = 'Submit a new link';
	
	$lang['TXT_CODING_ERROR'] = 'Something went wrong! Function: %s.';
	
	$lang['TXT_LONG_TIME'] = 'A Long time ago.';
	
	$lang['MONTH_AGO'] = "Month ago";
	$lang['MONTHS_AGO'] = "Months ago";
	
	$lang['YEAR_AGO'] = "Year ago";
	$lang['YEARS_AGO'] = "Years ago";
	
	$lang['DAY_AGO'] = "Day ago";
	$lang['DAYS_AGO'] = "Days ago";
	
	$lang['WEEK_AGO'] = "Week ago";
	$lang['WEEKS_AGO'] = "Weeks ago";
	
	$lang['HOUR_AGO'] = "Hour ago";
	$lang['HOURS_AGO'] = "Hours ago";
	
	$lang['MINUTE_AGO'] = "Minute ago";
	$lang['MINUTES_AGO'] = "Minutes ago";
	
	$lang['SECOND_AGO'] = "Second ago";
	$lang['SECONDS_AGO'] = "Seconds ago";
	
	
