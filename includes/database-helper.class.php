<?php

	if(!IN_SITE)
	{
		exit;
	}

	abstract class DatabaseHelper extends DatabaseLayer
	{
		//COUNT
		public function countSimpleSearchByName($tablename,$name,$search)
		{
			if($search === '') return false;
			
			$sql = 'SELECT COUNT(*) AS `total` FROM `'.$tablename.'` WHERE `'.$name.'` LIKE CONCAT(\'%\',?,\'%\')';
			$sql_params = [ [$search,'s'] ];
			$rows = $this->fetchAssoc($sql,$sql_params,false);
			
			return $rows['total'];
		}
		
		//INSERT
		//-----------------------------------------------------------------------------------------------------------------------------
		public function insertUser($email,$openid = true,$password = '')
		{
			if($openid){
			
				$sql = 'INSERT INTO `'.SQL_DB_USERS.'`(`id_session`,`ip`,`email`,`updated`,`openids`) VALUES(?,?,?,?,1)';
				$sql_params = [ [session_id(),'s'], [inet_pton(getIP()),'l'], [$email,'s'], [time(),'i'] ];
			}
			else{
				
				$sql = 'INSERT INTO `'.SQL_DB_USERS.'`(`id_session`,`ip`,`email`,`password`,`updated`) VALUES(?,?,?,?,?)';
				$sql_params = [ [session_id(),'s'], [inet_pton(getIP()),'l'], [$email,'s'], [$password,'s'], [time(),'i'] ];
			}
			
			return $this->executeQuery($sql,$sql_params);
		}
		
		public function insertOpenId($id,$openid,$provider)
		{
			$sql = 'INSERT INTO `'.SQL_DB_USERS_OPENIDS.'`(`id_user`,`openid`,`provider`) VALUES(?,?,?)';
			$sql_params = [ [$id,'i'], [$openid,'s'], [$provider,'s'] ];
			
			return $this->executeQuery($sql,$sql_params);
		}
		
		public function insertContributionRating($id,$id_user,$vote)
		{
			$sql = 'INSERT INTO `'.SQL_DB_CONTRIBUTIONS_RATINGS.'`(`id_contribution`,`id_user`,`voted`) VALUES(?,?,?)';
			$sql_params = [ [$id,'i'], [$id_user,'i'], [$vote,'i'] ];
			
			return $this->executeQuery($sql,$sql_params);
		}
		
		public function insertContribution($id_category,$id_user,$title,$url)
		{
			$sql = 'INSERT INTO `'.SQL_DB_CONTRIBUTIONS.'`(`id_category`,`id_user`,`title`,`url`) VALUES(?,?,?,?)';
			$sql_params = [ [$id_category,'i'], [$id_user,'i'], [$title,'s'], [$url,'s'] ];
			
			return $this->executeQuery($sql,$sql_params);
		}
		
		//SELECT
		//-----------------------------------------------------------------------------------------------------------------------------
		
		public function selectContributionRatingByIdCount($id,$limit, $select = '*')
		{
			$sql = 'SELECT '.$select.' FROM `'.SQL_DB_CONTRIBUTIONS_RATINGS.'` WHERE `id_contribution`=? LIMIT ?';
			$sql_params = [ [$id,'i'], [$limit,'i'] ];
			
			return $this->fetchAssoc($sql,$sql_params,true);
		}
		
		public function selectCategoriesByCount($limit, $select = '*')
		{
			$sql = 'SELECT '.$select.' FROM `'.SQL_DB_CATEGORIES.'` ORDER BY `arrange` ASC, `urlname` ASC LIMIT ?';
			$sql_params = [ [$limit,'i'] ];
			
			return $this->fetchAssoc($sql,$sql_params,true);
		}
		
		public function selectCategorieBySearchLimit($search,$limit,$maxRows)
		{
			$sql = 'SELECT * FROM `'.SQL_DB_CATEGORIES.'` WHERE `urlname` LIKE CONCAT(\'%\',?,\'%\') ORDER BY `urlname` ASC LIMIT ?, ?';
			$sql_params = [ [$search,'s'], [$limit,'i'], [$maxRows,'i'] ];
			
			return $this->fetchAssoc($sql,$sql_params,true);
		}
		
		public function selectOpenIdUser($id,$identity,$select = '*')
		{
			$sql 	= 'SELECT '.$select.' FROM `'.SQL_DB_USERS_OPENIDS.'` WHERE `id_user`=? AND `openid`=? LIMIT 1';
			$sql_params = [ [$id,'i'], [$identity,'s'] ];
			
			return $this->fetchAssoc($sql,$sql_params);
		}
		
		public function selectOpenIdsByUserId($id,$limit,$select = '*')
		{
			$sql 	= 'SELECT '.$select.' FROM `'.SQL_DB_USERS_OPENIDS.'` WHERE `id_user`=? LIMIT ?';
			$sql_params = [ [$id,'i'], [$limit,'i'] ];
			
			return $this->fetchAssoc($sql,$sql_params,true);
		}
		
		public function selectOpenIdByUserIdProvider($id,$provider,$select = '*')
		{
			$sql 	= 'SELECT '.$select.' FROM `'.SQL_DB_USERS_OPENIDS.'` WHERE `id_user`=? AND `provider`=? LIMIT 1';
			$sql_params = [ [$id,'i'], [$provider,'s'] ];
			
			return $this->fetchAssoc($sql,$sql_params);
		}
		
		public function selectUserByOpenIdEmail($email,$identity,$select = 'u.*')
		{
			$sql 	= 'SELECT '.$select.' FROM `'.SQL_DB_USERS.'` AS u 
				RIGHT JOIN `'.SQL_DB_USERS_OPENIDS.'` AS o ON o.`id_user`=u.`id` 
				WHERE u.`email`=? AND o.`openid`=? LIMIT 1';
				
			$sql_params = [ [$email,'s'], [$identity,'s'] ];
			return $this->fetchAssoc($sql,$sql_params);
		}
		
		public function selectUserByEmailUsername($email,$username,$password = true,$select = '*')
		{
			$password = ($password) ? ' AND `password` IS NOT NULL' : '';
		
			$sql = 'SELECT '.$select.' FROM `'.SQL_DB_USERS.'` WHERE (`email`=? OR `username`=?)'.$password.' LIMIT 1';
			$sql_params = [ [$email,'s'], [$username,'s'] ];
			
			return $this->fetchAssoc($sql,$sql_params);
		}
		
		public function selectContributionRating($id,$id_user,$select = '*')
		{
			$sql 	= 'SELECT '.$select.' FROM `'.SQL_DB_CONTRIBUTIONS_RATINGS.'` WHERE `id_contribution`=? AND `id_user`=? LIMIT 1';
			$sql_params = [ [$id,'i'], [$id_user,'i'] ];
			
			return $this->fetchAssoc($sql,$sql_params);
		}
		
		public function selectCategoryById($id,$select = '*')
		{
			return $this->fetchRowById(SQL_DB_CATEGORIES,$id,$select);
		}
		
		public function selectCategoryByName($id,$select = '*')
		{
			return $this->fetchRowByColumn(SQL_DB_CATEGORIES,'urlname',$id,$select);
		}
		
		public function selectUserById($id,$select = '*')
		{
			return $this->fetchRowById(SQL_DB_USERS,$id,$select);
		}
		
		public function selectUserByEmail($address,$select = '*')
		{
			return $this->fetchRowByColumn(SQL_DB_USERS,'email',$address,$select);
		}
		
		public function selectUserByUsername($name,$select = '*')
		{
			return $this->fetchRowByColumn(SQL_DB_USERS,'username',$name,$select);
		}
		
		public function selectTablesRows($name)
		{
			return $this->fetchRowByColumn(SQL_DB_TABLESROWS,'tablename',$name);
		}
		
		//UPDATE
		//-----------------------------------------------------------------------------------------------------------------------------
		
		public function updateUserColumnById($id,$name,$value,$datatype = 's')
		{
			$sql = 'UPDATE `'.SQL_DB_USERS.'` SET `'.$name.'`=? WHERE `id`=? LIMIT 1';
			$sql_params = [ [$value,$datatype], [$id,'i'] ];
			
			return $this->executeQuery($sql,$sql_params);
		}
		
		public function updateLogin($id)
		{
			$sql = 'UPDATE `'.SQL_DB_USERS.'` SET `id_session`=?, `ip`=? WHERE `id`=? LIMIT 1';
			$sql_params = [ [session_id(),'s'], [inet_pton(getIP()),'l'], [$id,'i'] ];
			
			return $this->executeQuery($sql,$sql_params);
		}
		
		public function updateContributionScore($id,$positive,$negative)
		{
			$sql = 'UPDATE `'.SQL_DB_CONTRIBUTIONS.'` SET `score_positive`=?, `score_negative`=? WHERE `id`=? LIMIT 1';
			$sql_params = [ [$positive,'i'], [$negative,'i'], [$id,'i'] ];
			
			return $this->executeQuery($sql,$sql_params);
		}
		
		public function updateContributionRatingById($id,$vote)
		{
			$sql = 'UPDATE `'.SQL_DB_CONTRIBUTIONS_RATINGS.'` SET `voted`=? WHERE `id`=? LIMIT 1';
			
			$sql_params = [ [$vote,'i'], [$id,'i'] ];
			
			return $this->executeQuery($sql,$sql_params);
		}
		
		public function updateCategoryContributions($id,$amount)
		{
			$sql = 'UPDATE `'.SQL_DB_CATEGORIES.'` SET `contributions`=? WHERE `id`=? LIMIT 1';
			$sql_params = [ [$amount,'i'], [$id,'i'] ];
			
			return $this->executeQuery($sql,$sql_params);
		}
		
		public function updateDBSessionId($id,$session)
		{
			$sql = 'UPDATE `'.SQL_DB_USERS.'` SET `id_session`=? WHERE `id`=? LIMIT 1';
			$sql_params = [ [$session,'s'], [$id,'i'] ];
			
			return $this->executeQuery($sql,$sql_params);
		}
		
		private function updateRows($tablename,$rows,$type)
		{
			$sql = 'UPDATE `'.SQL_DB_TABLESROWS.'` SET `rows`=`rows`+?, `'.$type.'`=NOW() WHERE `tablename`=? LIMIT 1';
			$sql_params = [ [$rows,'i'], [$tablename,'s'] ];
			
			return $this->executeQuery($sql,$sql_params);
		}
		
		public function updateAddRows($tablename,$rows)
		{
			$rows = (int)$rows;
			if($rows === 0)
				return true;
			
			$type = ($rows > 0) ? 'inserted' : 'deleted';
		
			return $this->updateRows($tablename,$rows,$type);
		}
		
		//SELECT (Complex)
		//-----------------------------------------------------------------------------------------------------------------------------
		
		public function selectComplexContributions($page,array $options,$maxRows)
		{
			$total = 0;
			$count = false;
			
			$sql_selects = [];
			
			$sql_where_conditions = [];
			$sql_where_conditions['AND'] = [];
			$sql_where_params = [];
			$sql_where_params['AND'] = [];
			
			$sql_joins = [];
			$sql_joins_params = [];
			
			if(!empty($options['owner'])){
			
				$options['owner'] = (int)$options['owner'];
			
				$sql_where_conditions['AND'][] = 'cb.`id_user`=?';
				$sql_where_params['AND'][] = [$options['owner'] , 'i'];
				$count = true;
			}
			else
			{
				$sql_selects[] = 'u.`username`, u.`id` AS `uid`';
				$sql_joins[] = 'LEFT JOIN `'.SQL_DB_USERS.'` AS u ON cb.`id_user`=u.`id`';
			}
			
			if(!empty($options['category'])){
			
				$options['category'] = (int)$options['category'];
			
				$sql_where_conditions['AND'][] = 'cb.`id_category`=?';
				$sql_where_params['AND'][] = [$options['category'] , 'i'];
				$count = true;
			}
			else
			{
				$sql_selects[] = 'c.`urlname` AS `category`, c.`id` AS `cid`';
				$sql_joins[] = 'LEFT JOIN `'.SQL_DB_CATEGORIES.'` as c ON cb.`id_category`=c.`id`';
			}
			
			if(!empty($options['voter'])){
				
				$options['voter'] = (int)$options['voter'];
				
				$sql_selects[] = 'r.`voted`';
				$sql_joins[] = 'LEFT JOIN `'.SQL_DB_CONTRIBUTIONS_RATINGS.'` AS r ON cb.`id`=r.`id_contribution` AND r.`id_user`=?';
				$sql_joins_params[] = [$options['voter'] , 'i'];
			}
			
			if(isset($options['search']) && $options['search'] !== ''){
			
				$sql_where_conditions['AND'][] = 'cb.`title` LIKE CONCAT(\'%\',?,\'%\')';
				$sql_where_params['AND'][] = [$options['search'] , 's'];
				$count = true;
			}
			
			if($count){
			
				$sql_where = '';
				if(!empty($sql_where_conditions['AND'])){
				
					$sql_where =	' WHERE ';
					$sql_where .=	implode(' AND ',$sql_where_conditions['AND']);
				}
				
				$sql = 'SELECT COUNT(*) AS `total` FROM `'.SQL_DB_CONTRIBUTIONS.'` AS cb'.$sql_where;
				
				$sql_params = [];
				$sql_params = array_merge($sql_params,$sql_where_params['AND']);
				$rows = $this->fetchAssoc($sql,$sql_params,false);
				$total = $rows['total'];
			}
			else if(isset($options['total'])){
			
				$total = (int) $options['total'];
			}
			else
			{
				$contribution = $this->selectTablesRows(SQL_DB_CONTRIBUTIONS);
				$total = $contribution['rows'];
			}
			
			$sort = ifsetor($options['sort']);
			switch($sort)
			{
				case 'hot':
					$sql_selects[] = '
					
							LOG10(GREATEST(ABS(CAST(cb.`score_positive` AS SIGNED) - CAST(cb.`score_negative` AS SIGNED)),1)) AS `hot`, 
							((UNIX_TIMESTAMP(cb.`created`) - UNIX_TIMESTAMP(\''.TS_FIRST_CONTRIBUTION.'\'))/45000) AS `time_hot`';
					
					$sql_orderBy = ' ROUND((`hot`+`time_hot`)*`sign`,7) DESC, cb.`created` DESC';
					break;
				case 'top':
					$sql_selects[] = '
					
							(CAST(cb.`score_positive` AS SIGNED) - CAST(cb.`score_negative` AS SIGNED)) AS `top`';
						
					$sql_orderBy = ' `top` DESC, cb.`created` DESC';
					break;
				case 'rising':
					$sql_selects[] = '
				
							LOG10(ABS(CAST(cb.`score_positive` AS SIGNED) - CAST(cb.`score_negative` AS SIGNED))) AS `rising`, 
							TIMEDIFF(DATE_ADD(cb.`created`,INTERVAL 12 HOUR),NOW())/45000 AS `time_rising`';
					
					$sql_orderBy = ' ROUND((`rising`*`sign`+`time_rising`),7) DESC, cb.`created` DESC';
					break;
				case 'controversial':
					$sql_selects[] = '
					
							LOG10(cb.`score_positive` + cb.`score_negative`)  AS `controversial`, 
							GREATEST(TIMEDIFF(DATE_ADD(cb.`created`,INTERVAL 1 DAY),NOW())/45000,0) AS `time_controversial`';
							
					$sql_orderBy = ' ROUND((`controversial`+`time_controversial`),7) DESC, cb.`created` DESC';
					break;
				default:
					$sql_orderBy = ' cb.`created` DESC';
			}
			
			$pages = vPageLinks($page,$total,$maxRows);
			
			$sql_select = '';
			if(!empty($sql_selects)){
			
				$sql_select = ', ';
				$sql_select .=	implode(', ',$sql_selects).' ';
			}
			
			$sql_join = '';
			if(!empty($sql_joins))
				$sql_join =	implode(' ',$sql_joins).' ';
			
			$sql_where = '';
			if(!empty($sql_where_conditions['AND'])){
			
				$sql_where =	'WHERE ';
				$sql_where .=	implode(' AND ',$sql_where_conditions['AND']).' ';
			}
			

			
			$sql = 'SELECT cb.*,UNIX_TIMESTAMP(cb.`created`) AS `utime`, 
				CAST(cb.`score_positive` AS SIGNED) - CAST(cb.`score_negative` AS SIGNED) AS `score`, 
				cb.`score_positive` + cb.`score_negative` AS `votes`, 
				
				CASE 
					WHEN CAST(cb.`score_positive` AS SIGNED) - CAST(cb.`score_negative` AS SIGNED) > 0 THEN 1 
					WHEN CAST(cb.`score_positive` AS SIGNED) - CAST(cb.`score_negative` AS SIGNED) < 0 THEN -1 
					ELSE 0 
				END AS `sign`
				
				'.$sql_select.'
				FROM `'.SQL_DB_CONTRIBUTIONS.'` AS cb '.$sql_join.$sql_where.'
				ORDER BY '.$sql_orderBy.' LIMIT ?, ?';
				
			$sql_params = [];
			$sql_params = array_merge($sql_params,$sql_joins_params,$sql_where_params['AND']);
			
			$sql_params[] = [$pages['limit'],'i'];
			$sql_params[] = [$pages['max_rows'],'i'];
				
			$contributions = $this->fetchAssoc($sql,$sql_params,true);
			
			if($contributions)
				return [$pages,$contributions];
			
			return [$pages,[]];
		}
	}
	
