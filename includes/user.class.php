<?php

	if(!defined('IN_SITE'))
	{
		exit;
	}
	
	class User extends Main
	{
		private $id = 0;
		private $id_session = '0';
		
		private $ip = '';
		
		private $username = '';
		private $password = '';
		private $email	  = '';

		private $firstname	= '';
		private $lastname	= '';
		private $gender		= 0;
		private $birthdate  = '';
		
		private $description = '';
		
		private $karma_contribution_positive = 0;
		private $karma_contribution_negative = 0;
		
		private $votes_contribution_positive = 0;
		private $votes_contribution_negative = 0;
		
		private $openids		= 0;
		private $contributions	= 0;
		private $comments		= 0;
		
		private $show_firstname	= 0;
		private $show_lastname	= 0;

		private $updated 	= 0;
		private $created 	= null;
		
		private $save = [];
		
		private function ifValueSetProperty(array $values)
		{
			foreach($values as $property => $value)
			{
				if(property_exists($this,$property))
					$this->$property = $value;
			}
		}

		function __construct($id = null,$select = '*')
		{
			parent::__construct();
			$this->getById($id,$select);
		}
		
		public function getById($id,$select)
		{
			$id = (int)$id;
			if($id < 1) return false;
			
			$user = $this->dbHelper->selectUserById($id,$select);
			if($user){
			
				$this->ifValueSetProperty($user);
				return $user;
			}
				
			return false;
		}

		public function ip()
		{
			if($this->ip !== "")
				return inet_ntop($this->ip);
			
			return '';
		}
		
		public function id(){return $this->id;}
		public function id_session(){return $this->id_session;}
		public function username(){return $this->username;}
		public function password(){return $this->password;}
		public function email(){return $this->email;}
		public function firstname(){return $this->firstname;}
		public function lastname(){return $this->lastname;}
		public function gender(){return $this->gender;}
		public function birthdate(){return $this->birthdate;}
		public function description(){return $this->description;}
		
		public function show_firstname(){return $this->show_firstname;}
		public function show_lastname(){return $this->show_lastname;}

		public function karma_contribution_positive(){return $this->karma_contribution_positive;}
		public function karma_contribution_negative(){return $this->karma_contribution_negative;}
		public function karma_contribution(){return $this->karma_contribution_positive - $this->karma_contribution_negative;}
		
		public function votes_contribution_positive(){return $this->votes_contribution_positive;}
		public function votes_contribution_negative(){return $this->votes_contribution_negative;}
		public function votes_contribution(){return $this->votes_contribution_positive + $this->votes_contribution_negative;}
		
		public function openids(){return $this->openids;}
		public function contributions(){return $this->contributions;}
		public function comments(){return $this->comments;}
		
		public function setShowFirstname($amount)
		{
			$this->save['show_firstname'] = '`show_firstname`=?';
			$this->show_firstname = (int)$amount;
			return true;
		}
		
		public function setShowLastname($amount)
		{
			$this->save['show_lastname'] = '`show_lastname`=?';
			$this->show_lastname = (int)$amount;
			return true;
		}
		
		public function setContributions($amount)
		{
			$this->save['contributions'] = '`contributions`=?';
			$this->contributions = (int)$amount;
			return true;
		}
		
		public function setOpenIds($amount)
		{
			$this->save['openids'] = '`openids`=?';
			$this->openids = (int)$amount;
			return true;
		}
		
		public function setKarmaContributionPositive($amount)
		{
			$this->save['karma_contribution_positive'] = '`karma_contribution_positive`=?';
			$this->karma_contribution_positive = (int)$amount;
			return true;
		}
		
		public function setKarmaContributionNegative($amount)
		{
			$this->save['karma_contribution_negative'] = '`karma_contribution_negative`=?';
			$this->karma_contribution_negative = (int)$amount;
			return true;
		}
		
		public function setVotesContributionPositive($amount)
		{
			$this->save['votes_contribution_positive'] = '`votes_contribution_positive`=?';
			$this->votes_contribution_positive = (int)$amount;
			return true;
		}
		
		public function setVotesContributionNegative($amount)
		{
			$this->save['votes_contribution_negative'] = '`votes_contribution_negative`=?';
			$this->votes_contribution_negative = (int)$amount;
			return true;
		}
		
		public function setEmail($address)
		{
			$errors_email = vEmail($address);
			if(!empty($errors_email)){
			
				$this->setErrorMessages($errors_email,'email');
				return false;
			}
			
			$this->save['email'] = '`email`=?';
			$this->email = $address;
			return true;
		}
		
		public function setPassword($password,$passwordConfirmation,$passwordOld)
		{
			$errors_password = vPassword($password,$passwordConfirmation);
			if(!empty($errors_password)){
			
				$this->setErrorMessages($errors_password,'password_new');
				return false;
			}
			
			if($this->password && !password_verify(getPassword($passwordOld,$this->email),$this->password)){
			
				$this->setErrorMessages(_translate('TXT_PASSWORD_WRONG'),'password_old');
				return false;
			}
			
			$this->save['password'] = '`password`=?';
			$this->password = hash_password($password,$this->email);
			return true;
		}
		
		public function setUsername($name)
		{
			$name = ($name === '') ? null : $name;
			if($name !== null){
			
				$errors_username = vUsername($name);
				if(!empty($errors_username)){
				
					$this->setErrorMessages($errors_username,'username');
					return false;
				}
			
				$user = $this->dbHelper->selectUserByUsername($name,'`id`');
				if($user && $user['id']){
				
					$this->setErrorMessages(_translate('TXT_USERNAME_EXISTS'),'username');
					return false;
				}
			}
			
			$this->save['username'] = '`username`=?';
			$this->username = $name;
			return true;
		}
		
		public function setFirstname($name)
		{
			$name = ($name === '') ? null : $name;
			if($name !== null){
			
				$errors_firstname = vFirstname($name);
				if(!empty($errors_firstname)){
				
					$this->setErrorMessages($errors_firstname,'firstname');
					return false;
				}
			}
			
			$this->save['firstname'] = '`firstname`=?';
			$this->firstname = $name;
			return true;
		}
		
		public function setLastname($name)
		{
			$name = ($name === '') ? null : $name;
			if($name !== null){
			
				$errors_lastname = vLastname($name);
				if(!empty($errors_lastname)){
				
					$this->setErrorMessages($errors_lastname,'lastname');
					return false;
				}
			}
			
			$this->save['lastname'] = '`lastname`=?';
			$this->lastname = $name;
			return true;
		}
		
		public function setDescription($str)
		{
			$errors_description = vDescription($str);
			if(!empty($errors_description)){
		
				$this->setErrorMessages($errors_description,'description');
				return false;
			}
			
			$this->save['description'] = '`description`=?';
			$this->description = $str;
		}
		
		private function toSave($keep)
		{
			switch($keep)
			{
				case 'username'			: return (($this->username) 	? [$this->username,'s'] : null);
				case 'email' 			: return [$this->email,'s'];
				case 'password'			: return [$this->password,'s'];
				case 'firstname'		: return (($this->firstname) 	? [$this->firstname,'s'] : null);
				case 'lastname'			: return (($this->lastname)		? [$this->lastname,'s'] : null);
				case 'description'		: return [$this->description,'s'];
				case 'show_firstname'	: return [$this->show_firstname,'i'];
				case 'show_lastname'	: return [$this->show_lastname,'i'];
				case 'contributions'	: return [$this->contributions,'i'];
				case 'openids'			: return [$this->openids,'i'];
				
				case 'karma_contribution_positive'	: return [$this->karma_contribution_positive,'i'];
				case 'karma_contribution_negative'	: return [$this->karma_contribution_negative,'i'];
				case 'votes_contribution_positive'	: return [$this->votes_contribution_positive,'i'];
				case 'votes_contribution_negative'	: return [$this->votes_contribution_negative,'i'];	
			}
	
			return false;
		}
		
		public function save()
		{
			if(!empty($this->save)){
			
				$toSave = implode(',',$this->save);
				$sql = 'UPDATE `'.SQL_DB_USERS.'` SET '.$toSave.' WHERE `id`=? LIMIT 1';
				
				$sql_params = [];
				foreach($this->save as $key => $value)
				{
					$param = $this->toSave($key);
					if($param === false) return false;
					
					$sql_params[] = $param;
				}
				
				$sql_params[] = [$this->id,'i'];
				
				$executed = $this->dbHelper->executeQuery($sql,$sql_params);
				if($executed){
					
					$this->save = [];
					return true;
				}
				
				return false;
			}
			
			return 0;
		}
		
		public function deleteContribution($id)
		{
			$contribution = $this->dbHelper->fetchRowById(SQL_DB_CONTRIBUTIONS,$id,'`id`,`id_user`,`score_positive`,`score_negative`');
			if($contribution['id_user'] === $this->id){
			
				$total = $contribution['score_positive'] + $contribution['score_negative'];
				$votes = $this->dbHelper->selectContributionRatingByIdCount($contribution['id'],$total);
				if($votes === false) return false;
				
				$this->dbHelper->beginTransaction();
			
				$updateVotes = true;
				if($votes)
				{
					$sql = 'UPDATE `'.SQL_DB_USERS.'` SET 
						`votes_contribution_positive`=`votes_contribution_positive`-?, 
						`votes_contribution_negative`=`votes_contribution_negative`-? 
						WHERE `id`=? LIMIT 1';
					
					$stmt = $this->dbHelper->prepare($sql);
					if($stmt){
					
						$this->dbHelper->bindParam($stmt,1,$positive,'i');
						$this->dbHelper->bindParam($stmt,2,$negative,'i');
						$this->dbHelper->bindParam($stmt,3,$idUser,'i');
						
						foreach($votes as $vote)
						{
							if($vote['voted'] === 1){
								
								$positive = 1;
								$negative = 0;
							}
							else if($vote['voted'] === -1){
							
								$positive = 0;
								$negative = 1;
							}
							
							$idUser = $vote['id_user'];
							if(!$this->dbHelper->execute($stmt)){
								
								$updateVotes = false;
							}
						}
						
						$this->dbHelper->closeStatement($stmt);
					}
				}
				
				if($updateVotes){
				
					$this->setContributions($this->contributions - 1);
					$this->setKarmaContributionPositive($this->karma_contribution_positive - $contribution['score_positive']);
					$this->setKarmaContributionNegative($this->karma_contribution_negative - $contribution['score_negative']);
					
					if(
						$this->save() && 
						$this->dbHelper->updateAddRows(SQL_DB_CONTRIBUTIONS,-1) && 
						$this->dbHelper->updateAddRows(SQL_DB_CONTRIBUTIONS_RATINGS,-$total) && 
						$this->dbHelper->deleteRowById(SQL_DB_CONTRIBUTIONS,$contribution['id'])
					){
					
						$this->dbHelper->commit();
						return true;
					}
				}
				
				$this->dbHelper->rollBack();
			}
			
			return false;
		}
		
		public function voteContribution($id,$positive)
		{
			$positive = ($positive) ? 1 : -1;
			$contribution = $this->dbHelper->fetchRowById(SQL_DB_CONTRIBUTIONS,$id,'`id`,`id_user`,`score_positive`,`score_negative`');
			if($contribution && $contribution['id']){
			
				if($contribution['id_user'] === $this->id)
					return false;
			
				$user = new User($contribution['id_user']);
				if($user->id() < 1) return false;

				$score_positive = 0;
				$score_negative = 0;
			
				$remove = false;
				$score = $this->dbHelper->selectContributionRating($contribution['id'],$this->id);
				if($score){
				
					if($score['voted'] === 1){
					
						$score_positive = -1;
						$user->setKarmaContributionPositive($user->karma_contribution_positive() - 1);
						$this->setVotesContributionPositive($this->votes_contribution_positive - 1);
						if($positive === 1)
							$remove = true;
						
					}
					else if($score['voted'] === -1){
					
						$score_negative = -1;
						$user->setKarmaContributionNegative($user->karma_contribution_negative() - 1);
						$this->setVotesContributionNegative($this->votes_contribution_negative - 1);
						if($positive === -1)
							$remove = true;
					}
				}
				
				$this->dbHelper->beginTransaction();
				$voted = false;
				if(!$remove){
				
					if($positive === 1){
					
						$score_positive = 1;
						$user->setKarmaContributionPositive($user->karma_contribution_positive() + 1);
						$this->setVotesContributionPositive($this->votes_contribution_positive + 1);
					}
					else
					{
						$score_negative = 1;
						$user->setKarmaContributionNegative($user->karma_contribution_negative() + 1);
						$this->setVotesContributionNegative($this->votes_contribution_negative + 1);
					}
					
					if(!$score){
					
						if($this->dbHelper->updateAddRows(SQL_DB_CONTRIBUTIONS_RATINGS,1))
							$voted = $this->dbHelper->insertContributionRating($contribution['id'],$this->id,$positive);
					}
					else
						$voted = $this->dbHelper->updateContributionRatingById($score['id'],$positive);
				}
				else
					$voted = $this->dbHelper->updateContributionRatingById($score['id'],null);
				
				$updated = $this->dbHelper->updateContributionScore($contribution['id'],
					$contribution['score_positive'] + $score_positive,
					$contribution['score_negative'] + $score_negative);
				
				if($user->save() && $this->save() && $voted && $updated){
				
					$this->dbHelper->commit();
					return ($score_positive - $score_negative);
				}
				
				$this->dbHelper->rollBack();
			}
			
			return false;
		}
		
		public function addRemoveOpenId($identity,$email)
		{
			if($this->email != $email)
			{
				$this->setErrorMessages(_translate('TXT_OPENID_EMAIL_MISMATCH'),'openid');
				return false;
			}
		
			$provider = parse_url($identity)['host'];
			$openid = $this->getOpenId($provider);
			if(!$this->password && $this->openids < 2 && $openid)
			{
				$this->setErrorMessages(_translate('TXT_OPENID_UNLINK_REQ'),'openid');
				return false;
			}

			$updated = false;
			$this->dbHelper->beginTransaction();
			if(!$openid)
			{
				$identity = hash_openid($identity,$this->email);
			
				$this->setOpenIds($this->openids + 1);
				if($this->save() && 
					$this->dbHelper->updateAddRows(SQL_DB_USERS_OPENIDS,1) && 
					$this->dbHelper->insertOpenId($this->id,$identity,$provider))
						$updated = true;
			}
			else
			{
				$this->setOpenIds($this->openids - 1);
				if($this->save() && 
					$this->dbHelper->updateAddRows(SQL_DB_USERS_OPENIDS,-1) && 
					$this->dbHelper->deleteRowById(SQL_DB_USERS_OPENIDS,$openid['id']))
						$updated = true;
			}
			
			if($updated)
			{
				$this->dbHelper->commit();
				return true;
			}
			
			$this->dbHelper->rollBack();
			return false;
		}
		
		public function getOpenIdProviders()
		{
			return $this->dbHelper->selectOpenIdsByUserId($this->id,$this->openids);
		}
		
		public function getOpenId($provider)
		{
			return $this->dbHelper->selectOpenIdByUserIdProvider($this->id,$provider);
		}
		
		public function getContributions($page = 1,$search = '', $sort = 'new', $voter = 0)
		{
			$options = [
			
				'total' 	=> $this->contributions,
				'owner' 	=> $this->id,
				'search'	=> $search,
				'sort' 		=> $sort,
				'voter' 	=> $voter
			];
			
			$maxRows = 10;
			
			return $this->dbHelper->selectComplexContributions($page,$options,$maxRows);
		}
		
		public function getProfileLink()
		{
			$url = 'profile/'.$this->id;
			if($this->username)
				$url .= '/'.rawurlencode($this->username);
			
			return $url;
		}
		
		public function getProfileName()
		{
			$name = _translate('USER').$this->id;
			if($this->username)
				$name = sanitizeSpChars($this->username);
				
			return $name;
		}
		
		public function setOnline($offset = 1)
		{
			if($this->id < 1 || $this->updated === null) return false;
		
			$time = time();
		
			if(($time > $this->online() + $offset)){
			
				$this->updated = $time;
				$executed = $this->dbHelper->updateUserColumnById($this->id,'updated',$time,'i');
				if($executed === 0){
				
					$sql = 'INSERT IGNORE INTO `'.SQL_DB_USERS_ONLINE.'`(`id`,`updated`) VALUES(?,?)';
					$sql_params = [	[$this->id,'i'] , [$time,'i'] ];
					if($this->dbHelper->executeQuery($sql,$sql_params) !== false)
						return true;
				}
			}
			
			return false;
		}

		public function online()
		{
			if($this->id < 1 || $this->updated === null) return false;
			
			$updated = $this->updated;
			$online = $this->dbHelper->fetchRowById(SQL_DB_USERS_ONLINE,$this->id,'`id`,`updated`');
			if($online){
			
				$bUpdated = false;
				$time = time();
				$timeout = $time - 3600;
				$updated = $online['updated'];
				
				if($updated < $timeout){
				
					$this->dbHelper->beginTransaction();
					$executed = $this->dbHelper->updateUserColumnById($this->id,'updated',$updated,'i');
					if($executed){
					
						$executed = $this->dbHelper->deleteRowById(SQL_DB_USERS_ONLINE,$online['id']);
						if($executed){
						
							$bUpdated = true;
							$this->dbHelper->commit();
						}
					}
					
					if(!$bUpdated) 
						$this->dbHelper->rollBack();
				}
			}
		
			return $updated;
		}
	}
	
	