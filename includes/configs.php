<?php

	if(!defined('IN_SITE'))
	{
		exit;
	}

	mb_internal_encoding('UTF-8');
	date_default_timezone_set('Europe/Stockholm');
	
	define('GOOGLE_ANALYTICS_ID', '');
	
	if(empty($_COOKIE["PHPSESSID"]))
		setcookie('PHPSESSID',hash('md5',uniqid()),0);
	
	ini_set('session.name','MEEMID');
	ini_set('session.hash_function','ripemd256');
	ini_set('session.hash_bits_per_character',6);
	ini_set('session.cookie_httponly',true);
	ini_set('session.referer_check', 'meemit.xuniver.se'); //change to your domain.
	
	define('BASE_PATH',realpath('.'));
	define('BASE_URL', dirname($_SERVER["SCRIPT_NAME"]));
	
	define('SALT_OPENID','DEFINE YOUR SALT');
	define('SALT_PASSWORD','DEFINE A EXTRA NONE UNIQUE SALT HERE');
	
	define('SESS_KEY_MENUSORT','menu_sort');
	define('SESS_KEY_SPAM','spam');
	define('SESS_KEY_USERID','userid');
	define('SESS_KEY_AUTHORIZED','authorized');
	define('SESS_KEY_URL_PREVIOUS','url_previous');
	
	define('SESS_KEY_OPENID','openid');
	define('SESS_KEY_OPENID_TOKEN','openid_token');
	
	define('SESS_KEY_H_HTTPUSERAGENT','hashed_httpUserAgent');
	define('SESS_KEY_H_REMOTEADDR','hashed_remoteAddr');

	define('DOCUMENT_TITLE',		'Meemit');
	define('DEFAULT_CATEGORY_ID',	1);
	define('DATE_FORMAT', 			'F jS, Y, g:i a');
	
	define('TS_FIRST_CONTRIBUTION', 	'2014-02-02 23:10:46'); //Define first post date for Hot sort algorithm.
	
	define('SQL_DB_CATEGORIES', 			'categories');
	define('SQL_DB_COMMENTS', 				'comments');
	define('SQL_DB_COMMENTS_RATINGS', 		'comments_ratings');
	define('SQL_DB_CONTRIBUTIONS', 			'contributions');
	define('SQL_DB_CONTRIBUTIONS_RATINGS', 	'contributions_ratings');
	define('SQL_DB_USERS', 					'users');
	define('SQL_DB_USERS_ONLINE', 			'users_online');
	define('SQL_DB_USERS_OPENIDS', 			'users_openids');
	define('SQL_DB_TABLESROWS', 			'tables_rows');
	
	define('DB_TYPE', 				'mysql');
	define('DB_HOST', 				'localhost');
	define('DB_USER',				'meemituser');
	define('DB_PASSWORD',			'password');
	define('DB_DATABASE',			'meemitdb');
	define('DB_CHARSET',			'utf8');
	define('DB_PORT',				3306);
	define('DB_PRINT_ERROR',		false);
	
	define('LENGTH_MAX_EMAIL', 			200);
	define('LENGTH_MAX_USERNAME', 		16);
	define('LENGTH_MAX_PASSWORD', 		1000);
	define('LENGTH_MAX_NAME', 			50);
	define('LENGTH_MAX_TITLE', 			96);
	define('LENGTH_MAX_URL', 			255);
	define('LENGTH_MAX_DESCRIPTION', 	10000);
	
	define('LENGTH_MIN_PASSWORD', 		6);
	define('LENGTH_MIN_EMAIL', 			5);
	define('LENGTH_MIN_USERNAME', 		3);
	define('LENGTH_MIN_NAME', 			2);
	define('LENGTH_MIN_TITLE', 			10);
	define('LENGTH_MIN_URL', 			11);
	define('LENGTH_MIN_DESCRIPTION', 	0);

