<?php

	/*
		Tested on PHP 5.5.8 (Linux) with 
		Curl
		Memcached Session
		GeoIP 1.0.8
		MySQLnd, PDO_MySQL (MySQL 5.6.15)
		XCache
	*/
	
	define('IN_SITE',true);
	
	require __DIR__ . '/includes/main.php';
	setSpamFilter('index');
	
	require __DIR__ . '/includes/category.class.php';
	require __DIR__ . '/includes/contribution.class.php';
	
	$g_contribution = new Contribution();
	
	$g_menuSort = [
		'hot' 				=> _translate('HOT'), 
		'new' 				=> _translate('NEW'),
		'rising' 			=> _translate('RISING'),
		'top' 				=> _translate('TOP'),
		'controversial' 	=> _translate('CONTROVERSIAL')];
	
	$g_getPage = ifSetOr($_GET['page'],'home');
	$g_getPage = page($g_getPage);
	
	$g_htmlKeywords 	= _translate('HTML_KEYWORDS');
	$g_htmlDescription 	= _translate('HTML_DESCRIPTION');
	
	$g_getCategory = ifsetor($_GET['category']);
	$g_category = new Category($g_getCategory);
	
	$g_sort = ifsetor($_SESSION[SESS_KEY_MENUSORT]);
	$g_getSort = ifsetor($_GET['sort'],$g_sort);
	
	if(isset($g_menuSort[$g_getSort])) $g_getSort = $g_getSort;
	else $g_getSort = key($g_menuSort);
	
	$_SESSION[SESS_KEY_MENUSORT] = $g_getSort;

	$g_previousURL = ifsetor($_SESSION[SESS_KEY_URL_PREVIOUS],$g_hostURL);
	if(empty($_POST)){
	
		if($g_previousURL != getCurrentURL()){
		
			$_SESSION[SESS_KEY_URL_PREVIOUS] = getCurrentURL();
		}
	}
	
	$g_categoryURL 	= $g_hostURL;
	$g_sortURL 		= $g_hostURL;
	
	$g_sortedURL = '/sort/'.$g_getSort;
	if($g_category->id() > 0)
		$g_sortedURL = '/category/'.$g_category->id().'/'.rawurlencode($g_category->urlname()).'/sort/'.rawurlencode($g_getSort);
	
	require __DIR__ . '/pages/' . $g_getPage . '.php';
	
	require __DIR__ . '/index.html.php';

