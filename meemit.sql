
CREATE TABLE `users`(
	`id` BIGINT unsigned NOT NULL AUTO_INCREMENT,
	`id_session` VARCHAR(64) NOT NULL,
	`ip` VARBINARY(16) NOT NULL,
	`email` VARCHAR(200) NOT NULL,
	`username` VARCHAR(16) DEFAULT NULL,
	`password` CHAR(60) DEFAULT NULL,
	`firstname` VARCHAR(50) DEFAULT NULL,
	`lastname` VARCHAR(50) DEFAULT NULL,
	`description` TEXT DEFAULT NULL,
	`openids` SMALLINT unsigned DEFAULT 0,
	
	`contributions` INT unsigned DEFAULT 0,
	`karma_contribution_positive` INT unsigned DEFAULT 0,
	`karma_contribution_negative` INT unsigned DEFAULT 0,
	`votes_contribution_positive` INT unsigned DEFAULT 0,
	`votes_contribution_negative` INT unsigned DEFAULT 0,
	
	`comments` INT unsigned DEFAULT 0,
	`karma_comments_positive` INT unsigned DEFAULT 0,
	`karma_comments_negative` INT unsigned DEFAULT 0,
	`votes_comments_positive` INT unsigned DEFAULT 0,
	`votes_comments_negative` INT unsigned DEFAULT 0,
	
	`show_firstname` TINYINT unsigned DEFAULT 0,
	`show_lastname` TINYINT unsigned DEFAULT 0,
	
	`updated` BIGINT DEFAULT 0,
	`created` TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
	PRIMARY KEY(`id`),
	UNIQUE(`email`),
	UNIQUE(`username`),
	UNIQUE(`password`)
) DEFAULT CHARACTER SET utf8;

CREATE TABLE `users_online`(
  `id` BIGINT unsigned NOT NULL,
  `updated` BIGINT NOT NULL,
	INDEX `id_user`(`id`),
	FOREIGN KEY (`id`)
        REFERENCES `users`(`id`) ON DELETE CASCADE
) DEFAULT CHARACTER SET utf8;

CREATE TABLE `users_openids`(
	`id` BIGINT unsigned NOT NULL AUTO_INCREMENT,
	`id_user` BIGINT unsigned NOT NULL,
	`openid` CHAR(128) NOT NULL,
	`provider` VARCHAR( 128 ) NOT NULL,
	`created` TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
	PRIMARY KEY(`id`),
	UNIQUE(`openid`),
	INDEX(`id_user`),
	FOREIGN KEY (`id_user`)
        REFERENCES `users`(`id`) ON DELETE CASCADE
) DEFAULT CHARACTER SET utf8;

CREATE TABLE `categories`(
	`id` INT unsigned NOT NULL AUTO_INCREMENT,
	`urlname` VARCHAR(28) DEFAULT NULL,
	`description` TEXT DEFAULT NULL,
	`arrange` INT unsigned DEFAULT NULL,
	`contributions` INT unsigned DEFAULT 0,
	`comments` BIGINT unsigned DEFAULT 0,
	`created` TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
	PRIMARY KEY(`id`),
	UNIQUE(`urlname`)
) DEFAULT CHARACTER SET utf8;

CREATE TABLE `contributions`(
	`id` BIGINT unsigned NOT NULL AUTO_INCREMENT,
	`id_category` INT unsigned NOT NULL,
	`id_user` BIGINT unsigned NOT NULL,
	`title` varchar(96) NOT NULL,
	`url` varchar(255) NOT NULL,
	`score_positive` INT unsigned DEFAULT 0,
	`score_negative` INT unsigned DEFAULT 0,
	`created` TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
	PRIMARY KEY(`id`),
	INDEX (`id_category`),
	FOREIGN KEY (`id_category`)
        REFERENCES `categories`(`id`) ON DELETE CASCADE,
	INDEX (`id_user`),
	FOREIGN KEY (`id_user`)
        REFERENCES `users`(`id`) ON DELETE CASCADE
) DEFAULT CHARACTER SET utf8;

CREATE TABLE `contributions_ratings`(
	`id` BIGINT unsigned NOT NULL AUTO_INCREMENT,
	`id_contribution` BIGINT unsigned NOT NULL,
	`id_user` BIGINT unsigned NOT NULL,
	`voted` TINYINT DEFAULT NULL,
	`created` TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
	PRIMARY KEY(`id`),
	INDEX (`id_contribution`),
	FOREIGN KEY (`id_contribution`)
        REFERENCES `contributions`(`id`) ON DELETE CASCADE,
	INDEX (`id_user`),
	FOREIGN KEY (`id_user`)
        REFERENCES `users`(`id`) ON DELETE CASCADE,
	UNIQUE `contributions_users`(`id_contribution`,`id_user`)
) DEFAULT CHARACTER SET utf8;

CREATE TABLE `tables_rows`(
	`id` SMALLINT unsigned NOT NULL AUTO_INCREMENT,
	`tablename` VARCHAR(128) NOT NULL,
	`rows` BIGINT unsigned DEFAULT 0,
	`inserted` TIMESTAMP NULL,
	`deleted` TIMESTAMP NULL,
	PRIMARY KEY (`id`),
	UNIQUE (`tablename`)
) DEFAULT CHARACTER SET utf8;

INSERT INTO `tables_rows` (`id`, `tablename`,`rows`) VALUES
	(1, 'categories',7),
	(2, 'comments',0),
	(3, 'comments_ratings',0),
	(4, 'contributions',0),
	(5, 'contributions_ratings',0),
	(6, 'users',0),
	(7, 'users_online',0),
	(8, 'users_openids',0);
	
INSERT INTO `categories` (`urlname`, `description`, `arrange`) VALUES
	('other','Post anything else here.', 0),
	('funny','You should only post if you are funny.', 4),
	('aww','Things that make you go AWW! -- like puppies, and kitties, and so on... Feel free to post pictures, videos and stories of cute things.', 3),
	('news','Post new information here.', 1),
	('pics','A place to share pictures and photographs.', 2),
	('videos','A great place for video content of all kinds. Direct links to major video streaming sites are preferred (e.g. YouTube, Vimeo, etc.)', 5),
	('music','Post anything releated to music.', 6);
	
