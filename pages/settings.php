<?php 

	if(!defined('IN_SITE'))
	{
		exit;
	}
	
	$g_title = _translate('SETTINGS');
	
	$saved = false;
	
	$username 		= $clientUser->username();
	$firstname 		= $clientUser->firstname();
	$lastname 		= $clientUser->lastname();
	$description 	= $clientUser->description();
	
	$descUsername 		= _translate('TXT_VALID_USERNAME');
	$descPassword 		= _translate('TXT_VALID_PASSWORD');
	$descPasswordOld 	= '';
	
	$classUsername		= '';
	$classPassword		= '';
	$classPasswordOld	= '';
	
	$descFirstname 		= _translate('TXT_VALID_NAME');
	$descLastname 		= sprintf(_translate('TXT_SAME_AS'),_translate('FIRSTNAME','LC'));
	$descDescription 	= '';
	
	$classFirstname		= '';
	$classLastname		= '';
	$classDescription	= '';
	
	$checkedFirstname 	= '';
	$checkedLastname 	= '';
	
	$submitChange = ifsetor($_POST['submitChange']);
	if($submitChange){
	
		$username 		= ifsetor($_POST['username']);
		$firstname 		= ifsetor($_POST['firstname']);
		$lastname 		= ifsetor($_POST['lastname']);
		$description 	= ifsetor($_POST['description']);
		
		$password 			= ifsetor($_POST['password'],'');
		$passwordConfirm 	= ifsetor($_POST['passwordConfirm']);
		$passwordOld 		= ifsetor($_POST['passwordOld']);
		
		$show_firstname = ifsetor($_POST['showFirstname']);
		$show_lastname 	= ifsetor($_POST['showLastname']);
		
		$show_firstname = ($show_firstname) ? 1 : 0;
		$show_lastname 	= ($show_lastname) 	? 1 : 0;

		if($username != $clientUser->username())
			$clientUser->setUsername($username);
			
		if($firstname != $clientUser->firstname())
			$clientUser->setFirstname($firstname);
			
		if($lastname != $clientUser->lastname())
			$clientUser->setLastname($lastname);
			
		if($description != $clientUser->description())
			$clientUser->setDescription($description);
			
		if($show_firstname != $clientUser->show_firstname())
			$clientUser->setShowFirstname($show_firstname);
			
		if($show_lastname != $clientUser->show_lastname())
			$clientUser->setShowLastname($show_lastname);
	
		if($password !== '')
			$clientUser->setPassword($password,$passwordConfirm,$passwordOld);
			
		$error = $clientUser->getFirstError('username');
		if($error) $classUsername = ' failure';
		$descUsername = ($error) ? $error : $descUsername;
		
		$error = $clientUser->getFirstError('firstname');
		if($error) $classFirstname = ' failure';
		$descFirstname = ($error) ? $error : $descFirstname;
		
		$error = $clientUser->getFirstError('lastname');
		if($error) $classLastname = ' failure';
		$descLastname = ($error) ? $error : $descLastname;
		
		$error = $clientUser->getFirstError('description');
		if($error) $classDescription = ' failure';
		$descDescription = ($error) ? $error : $descDescription;
		
		$error = $clientUser->getFirstError('password_new');
		if($error) $classPassword = ' failure';
		$descPassword = ($error) ? $error : $descPassword;
		
		$error = $clientUser->getFirstError('password_old');
		if($error) $classPasswordOld = ' failure';
		$descPasswordOld = ($error) ? $error : $descPasswordOld;
		
		$saved = $clientUser->save();
		
		$username 		= sanitizeSpChars($username);
		$firstname 		= sanitizeSpChars($firstname);
		$lastname 		= sanitizeSpChars($lastname);
		$description 	= sanitizeSpChars($description);
	}
	
	$openid_google = _translate('UNLINKED');
	
	$openids = $clientUser->getOpenIdProviders();
	foreach($openids as $openid)
	{
		switch($openid['provider'])
		{
			case Configs::OID_P_GOOGLE:
				$openid_google = _translate('LINKED');
				break;
		}
	}
	
	if($clientUser->show_lastname())
		$checkedLastname = ' checked';
		
	if($clientUser->show_firstname())
		$checkedFirstname = ' checked';
	
	